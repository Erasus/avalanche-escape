﻿using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.Events;

public class LocalizationManager : MonoBehaviour
{
    [SerializeField] private TextAsset _textFile;

    private Dictionary<string, List<string>> _localization;

    public int SelectedLanguage { get; private set; }

    public event UnityAction LanguageChange;
    public event UnityAction<int> SaveLanguageIndex;

    private void Awake()
    {
        if (_localization == null)
            LoadLocalization();
    }

    public void SetLanguage(int id)
    {
        SaveLanguageIndex?.Invoke(id);

        SelectedLanguage = id;
        LanguageChange?.Invoke();
    }

    public void LoadLanguageIndex(int index)
    {
        SetLanguage(index);
    }

    private void LoadLocalization()
    {
        _localization = new Dictionary<string, List<string>>();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(_textFile.text);

        foreach (XmlNode key in xmlDocument["Keys"].ChildNodes)
        {
            string keyStr = key.Attributes["Name"].Value;

            var values = new List<string>();
            foreach (XmlNode translate in key["Translates"].ChildNodes)
                values.Add(translate.InnerText);

            _localization[keyStr] = values;
        }
    }

    public string GetTranslate(string key, int languageId = -1)
    {
        if (languageId == -1)
            languageId = SelectedLanguage;

        if (_localization.ContainsKey(key))
            return _localization[key][languageId];

        return key;
    }
}
