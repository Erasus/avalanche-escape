﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class Parallax : MonoBehaviour
{
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [SerializeField] private DistanceCounter _distanceCounter;
    [SerializeField] private float _speed;
    [SerializeField] private float _standartAccelerationCoefficient = 1;

    private float _lastAccelerationCoefficient;
    private float _standartSpeed;
    private RawImage _image;
    private float _imagePositionY;

    private void Awake()
    {
        _image = GetComponent<RawImage>();
        _standartSpeed = _speed;
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
        StopSpeed();
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GameOver += OnGameOver;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _distanceCounter.SpeedIncreased += OnSpeedIncreased;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GameOver -= OnGameOver;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _distanceCounter.SpeedIncreased -= OnSpeedIncreased;
    }

    private void Update()
    {
        _imagePositionY -= _speed * _lastAccelerationCoefficient * Time.deltaTime;
        
        if (_imagePositionY <= -1)
            _imagePositionY = 0;

        _image.uvRect = new Rect(0, _imagePositionY, _image.uvRect.width, _image.uvRect.height);
    }

    private void OnGameBegin()
    {
        ResetSpeed();
    }
    
    private void OnGameOver()
    {
        StopSpeed();
    }
    private void OnReturningStartScreen()
    {
        OnGameOver();
    }

    private void OnSpeedIncreased(float accelerationCoefficient)
    {
        _lastAccelerationCoefficient += accelerationCoefficient;
    }

    private void StopSpeed()
    {
        _speed = 0;
    }

    private void ResetSpeed()
    {
        _speed = _standartSpeed;
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }
}
