﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Playables;

public class GameModeSwitcher : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private StartScreen _startScreen;
    [SerializeField] private GameOverScreen _gameOverScreen;
    [SerializeField] private PauseScreen _pauseScreen;
    [SerializeField] private OptionsScreen _optionsScreen;
    [SerializeField] private ExitGameScreen _exitGameScreen;
    [SerializeField] private Button _resumeGameButton;
    [SerializeField] private Button _returnStartScreenButton;
    [SerializeField] private Button _closeOptionsScreenButton;
    [SerializeField] private GameInterface _gameInterface;

    public int GameOverCount { get; private set; } = 0;
    public event UnityAction GameBegin;
    public event UnityAction GameOver;
    public event UnityAction GamePaused;
    public event UnityAction GameUnpaused;
    public event UnityAction ReturningStartScreen;
    public event UnityAction PlaceOnDisabledPosition;

    private void Start()
    {
        _gameOverScreen.Close();
        _pauseScreen.Close();
        _optionsScreen.Close();
        _exitGameScreen.Close();
        _startScreen.Open();
        _gameInterface.CloseInterface();
    }

    private void OnEnable()
    {
        _startScreen.PlayButtonClick += OnPlayButtonClick;
        _gameOverScreen.RestartButtonClick += OnRestartButtonClick;
        _pauseScreen.PauseButtonClick += OnPauseButtonClick;
        _startScreen.OpenOptionsButtonClick += OnOpenOptionsButtonClick;
        _gameOverScreen.OpenOptionsButtonClick += OnOpenOptionsButtonClick;
        _startScreen.OpenExitScreenButtonClick += OnOpenExitScreenButtonClick;
        _gameOverScreen.OpenExitScreenButtonClick += OnOpenExitScreenButtonClick;
        _pauseScreen.OpenExitScreenButtonClick += OnOpenExitScreenButtonClick;
        _optionsScreen.CloseOptionsButtonClick += OnCloseOptionsButtonClick;
        _exitGameScreen.CloseExitGameScreenButtonClick += OnCloseExitGameScreenButtonClick;
        _resumeGameButton.onClick.AddListener(ResumeGame);
        _returnStartScreenButton.onClick.AddListener(ReturnStartScreen);
        _player.Crashed += OnCrashed;
    }

    private void OnDisable()
    {
        _startScreen.PlayButtonClick -= OnPlayButtonClick;
        _gameOverScreen.RestartButtonClick -= OnRestartButtonClick;
        _pauseScreen.PauseButtonClick -= OnPauseButtonClick;
        _startScreen.OpenOptionsButtonClick -= OnOpenOptionsButtonClick;
        _gameOverScreen.OpenOptionsButtonClick -= OnOpenOptionsButtonClick;
        _startScreen.OpenExitScreenButtonClick -= OnOpenExitScreenButtonClick;
        _gameOverScreen.OpenExitScreenButtonClick -= OnOpenExitScreenButtonClick;
        _pauseScreen.OpenExitScreenButtonClick -= OnOpenExitScreenButtonClick;
        _optionsScreen.CloseOptionsButtonClick -= OnCloseOptionsButtonClick;
        _exitGameScreen.CloseExitGameScreenButtonClick -= OnCloseExitGameScreenButtonClick;
        _resumeGameButton.onClick.RemoveListener(ResumeGame);
        _returnStartScreenButton.onClick.RemoveListener(ReturnStartScreen);
        _player.Crashed -= OnCrashed;
    }

    private void OnPlayButtonClick()
    {
        _startScreen.Close();
        _gameInterface.OpenInterface();

        GameBegin?.Invoke();
    }
    
    private void OnRestartButtonClick()
    {
        _gameOverScreen.Close();
        _gameInterface.OpenInterface();

        PlaceOnDisabledPosition?.Invoke();
        GameBegin?.Invoke();
    }
    
    private void OnPauseButtonClick()
    {
        _pauseScreen.Open();
        _gameInterface.CloseInterface();

        GamePaused?.Invoke();
    }

    private void ResumeGame()
    {
        _pauseScreen.Close();
        _gameInterface.OpenInterface();

        GameUnpaused?.Invoke();
    }

    private void OnOpenOptionsButtonClick()
    {
        _optionsScreen.Open();
    }

    private void OnOpenExitScreenButtonClick()
    {
        _exitGameScreen.Open();
    }

    private void OnCloseOptionsButtonClick()
    {
        CheckPreviousScreen();
        _optionsScreen.Close();
    }

    private void OnCloseExitGameScreenButtonClick()
    {
        CheckPreviousScreen();
        _exitGameScreen.Close();
    }

    private void CheckPreviousScreen()
    {
        if (_startScreen.ScreenOpened)
            _startScreen.Open();
        else if (_gameOverScreen.ScreenOpened)
            _gameOverScreen.Open();
        else if (_pauseScreen.ScreenOpened)
            _pauseScreen.Open();
    }

    private void ReturnStartScreen()
    {
        _pauseScreen.Close();
        _gameInterface.CloseInterface();
        _startScreen.Open();

        PlaceOnDisabledPosition?.Invoke();
        ReturningStartScreen?.Invoke();
    }

    private void OnCrashed()
    {
        GameOverCount++;
        _gameInterface.CloseInterface();
        _gameOverScreen.Open();

        GameOver?.Invoke();
    }

    public void ResetGameOverCounter()
    {
        GameOverCount = 0;
    }
}
