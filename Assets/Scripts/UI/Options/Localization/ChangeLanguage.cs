﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ChangeLanguage : MonoBehaviour
{
    [SerializeField] private List<Sprite> _flagImages;
    [SerializeField] private LocalizationManager _localizationManager;

    private Button _languageButton;
    private int _index = 0;

    private void Awake()
    {
        _languageButton = GetComponent<Button>();
    }

    private void Start()
    {
        _index = _localizationManager.SelectedLanguage;
        _languageButton.image.sprite = _flagImages[_index];
    }

    private void OnEnable()
    {
        _languageButton.onClick.AddListener(LanguageChange);
    }

    private void OnDisable()
    {
        _languageButton.onClick.RemoveListener(LanguageChange);
    }

    private void LanguageChange()
    {
        if (_index < _flagImages.Count - 1)
            _index++;
        else if (_index > 0)
            _index = 0;

        _languageButton.image.sprite = _flagImages[_index];
        _localizationManager.SetLanguage(_index);
    }
}