﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class Statistic : MonoBehaviour
{
    [SerializeField] protected Image BestScoreImage;
    [SerializeField] protected TMP_Text BestDistanceText;
    [SerializeField] protected DistanceCounter DistanceCounter;

    public abstract void ShowBestScore();
}
