﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class DistanceCounter : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [SerializeField] private TMP_Text _distanceText;
    [SerializeField] private float _speed;
    [SerializeField] private float _standartAccelerationCoefficient = 1;
    [SerializeField, Range(0, 1)] private float _accelerationCoefficient;
    [SerializeField] private int _distanceForUpSpeed;

    private float _lastAccelerationCoefficient;
    private int _lastDistanceForUpSpeed;
    private float _distance = 0;
    private bool _gameStarted = false;

    public int Distance => (int)_distance;
    public int BestDistance { get; private set; }
    public event UnityAction<float> SpeedIncreased;
    public event UnityAction<int> SaveBestDistance;

    private void Start()
    {
        _lastDistanceForUpSpeed = _distanceForUpSpeed;
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _player.Crashed += OnCrashed;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _player.Crashed -= OnCrashed;
    }

    private void OnGameBegin()
    {
        ResetDistance();
        _gameStarted = true;
        ResetSpeed();
    }
    
    private void OnReturningStartScreen()
    {
        _gameStarted = false;
        CheckNewHighscore();
        ResetDistance();
    }
    
    private void OnCrashed()
    {
        _gameStarted = false;

        if (CheckNewHighscore())
            SetNewHighscore();
    }

    private void Update()
    {
        if(_gameStarted)
        {
            _distance += Time.deltaTime * _speed * _lastAccelerationCoefficient;
            _distanceText.text = ((int)_distance).ToString() + "м.";

            if (_distance >= _distanceForUpSpeed)
            {
                _distanceForUpSpeed += _lastDistanceForUpSpeed;
                UpSpeed();
            }
        }
    }

    private void UpSpeed()
    {
        _lastAccelerationCoefficient += _accelerationCoefficient;
        SpeedIncreased?.Invoke(_accelerationCoefficient);
    }

    private void ResetSpeed()
    {
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }

    public bool CheckNewHighscore()
    {
        return Distance > BestDistance;
    }

    private void SetNewHighscore()
    {
        BestDistance = Distance;
        SaveBestDistance?.Invoke(BestDistance);
    }

    private void ResetDistance()
    {
        _distance = 0;
        _distanceForUpSpeed = _lastDistanceForUpSpeed;
    }

    public void LoadBestDistance(int value)
    {
        BestDistance = value;
    }
}
