﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonKnockableCollision : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Player player))
        {
            player.Die();

            if(gameObject.GetComponent<Obstacle>().ObstacleType == ObstacleType.SnowBoulder)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
