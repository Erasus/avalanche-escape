﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(PauseScreenStatistic))]
public class PauseScreen : Screen
{
    [SerializeField] private Button _exitGameScreenButton;

    private PauseScreenStatistic _pauseScreenStatistic;

    public event UnityAction PauseButtonClick;
    public event UnityAction OpenExitScreenButtonClick;

    private void Awake()
    {
        _pauseScreenStatistic = GetComponent<PauseScreenStatistic>();
    }

    protected new void OnEnable()
    {
        base.OnEnable();
        _exitGameScreenButton.onClick.AddListener(OpenExitScreen);
    }

    protected new void OnDisable()
    {
        base.OnDisable();
        _exitGameScreenButton.onClick.RemoveListener(OpenExitScreen);
    }

    private void OpenExitScreen()
    {
        ScreenOpened = true;
        HideScreen();

        OpenExitScreenButtonClick?.Invoke();
    }

    public override void Close()
    {
        Time.timeScale = 1;

        HideScreen();

        Button.interactable = true;
        Button.image.raycastTarget = true;
        SwipePanel.raycastTarget = true;
    }

    private void HideScreen()
    {
        CanvasGroup.alpha = 0;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;
    }

    public override void Open()
    {
        Time.timeScale = 0;
        CanvasGroup.alpha = 1;
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;
        Button.interactable = false;
        Button.image.raycastTarget = false;
        SwipePanel.raycastTarget = false;
        _pauseScreenStatistic.ShowBestScore();
        ScreenOpened = false;
    }

    protected override void OnButtonClick()
    {
        PauseButtonClick?.Invoke();
    }
}
