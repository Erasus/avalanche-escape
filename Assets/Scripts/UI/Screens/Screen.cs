﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Screen : MonoBehaviour
{
    [SerializeField] protected CanvasGroup CanvasGroup;
    [SerializeField] protected Button Button;
    [SerializeField] protected Image SwipePanel;

    public bool ScreenOpened { get; protected set; } = false;

    protected void OnEnable()
    {
        Button.onClick.AddListener(OnButtonClick);
    }

    protected void OnDisable()
    {
        Button.onClick.RemoveListener(OnButtonClick);
    }

    public abstract void Open();
    public abstract void Close();
    protected abstract void OnButtonClick();
}
