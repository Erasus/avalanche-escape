﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(StartScreenStatistic))]
public class StartScreen : Screen
{
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _exitGameScreenButton;

    private StartScreenStatistic _startScreenStatistic;

    public event UnityAction PlayButtonClick;
    public event UnityAction OpenOptionsButtonClick;
    public event UnityAction OpenExitScreenButtonClick;

    private void Awake()
    {
        _startScreenStatistic = GetComponent<StartScreenStatistic>();
    }

    protected new void OnEnable()
    {
        base.OnEnable();
        _optionsButton.onClick.AddListener(OpenOptionsScreen);
        _exitGameScreenButton.onClick.AddListener(OpenExitScreen);
    }

    protected new void OnDisable()
    {
        base.OnDisable();
        _optionsButton.onClick.RemoveListener(OpenOptionsScreen);
        _exitGameScreenButton.onClick.RemoveListener(OpenExitScreen);
    }

    private void OpenOptionsScreen()
    {
        ScreenOpened = true;
        Close();

        OpenOptionsButtonClick?.Invoke();
    }

    private void OpenExitScreen()
    {
        ScreenOpened = true;
        Close();

        OpenExitScreenButtonClick?.Invoke();
    }

    public override void Close()
    {
        CanvasGroup.alpha = 0;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;
        SwipePanel.raycastTarget = true;
    }

    public override void Open()
    {
        CanvasGroup.alpha = 1;
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;
        SwipePanel.raycastTarget = false;
        _startScreenStatistic.ShowBestScore();
        ScreenOpened = false;
    }

    protected override void OnButtonClick()
    {
        PlayButtonClick?.Invoke();
    }
}
