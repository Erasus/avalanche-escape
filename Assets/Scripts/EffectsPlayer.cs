﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameModeSwitcher))]
public class EffectsPlayer : MonoBehaviour
{
    [SerializeField] private ParticleSystem _windEffect;

    private GameModeSwitcher _gameModeSwitcher;

    private void Awake()
    {
        _gameModeSwitcher = GetComponent<GameModeSwitcher>();
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GamePaused += OnGamePaused;
        _gameModeSwitcher.GameUnpaused += OnGameUnpaused;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _gameModeSwitcher.GameOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GamePaused -= OnGamePaused;
        _gameModeSwitcher.GameUnpaused -= OnGameUnpaused;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _gameModeSwitcher.GameOver -= OnGameOver;
    }

    private void OnGameBegin()
    {
        _windEffect.Play();
    }

    private void OnGamePaused()
    {
        _windEffect.Pause();
    }

    private void OnGameUnpaused()
    {
        _windEffect.Play();
    }

    private void OnReturningStartScreen()
    {
        _windEffect.Clear();
        _windEffect.Stop();
    }

    private void OnGameOver()
    {
        _windEffect.Stop();
    }
}
