﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class VolumeChanger : MonoBehaviour
{
    [SerializeField] private AudioSource[] _audioSource;
    [SerializeField] private Slider _slider;
    [SerializeField] private TMP_Text _volumeText;

    public event UnityAction<float> SaveVolume;

    private void Start()
    {
        ChangeVolume(_slider.value);
    }

    private void OnEnable()
    {
        _slider.onValueChanged.AddListener(ChangeVolume);
    }

    private void OnDisable()
    {
        _slider.onValueChanged.RemoveListener(ChangeVolume);
    }

    private void ChangeVolume(float value)
    {
        int volumeValue = (int)(value * 100);
        _volumeText.text = volumeValue.ToString();

        foreach (var sound in _audioSource)
        {
            sound.volume = value;
        }

        SaveVolume?.Invoke(value);
    }

    public void LoadVolume(float value)
    {
        _slider.value = value;
    }
}
