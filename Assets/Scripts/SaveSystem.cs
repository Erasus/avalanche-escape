﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SaveSystem : MonoBehaviour
{
    [SerializeField] private DistanceCounter _distanceCounter;
    [SerializeField] private LocalizationManager _localizationManager;
    [SerializeField] private VolumeChanger _volumeChanger;

    private Save _save = new Save();
    private string _path;

    private void Awake()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        _path = Path.Combine(Application.persistentDataPath, "Save.json");
#else
        _path = Path.Combine(Application.dataPath, "Save.json");
#endif

        if(File.Exists(_path))
        {
            _save = JsonUtility.FromJson<Save>(File.ReadAllText(_path));
            _distanceCounter.LoadBestDistance(_save.BestDistance);
            _localizationManager.LoadLanguageIndex(_save.LanguageIndex);
            _volumeChanger.LoadVolume(_save.Volume);
        }
    }

    private void OnEnable()
    {
        _distanceCounter.SaveBestDistance += OnSaveBestDistance;
        _localizationManager.SaveLanguageIndex += OnSaveLanguageIndex;
        _volumeChanger.SaveVolume += OnSaveVolume;
    }

    private void OnDisable()
    {
        _distanceCounter.SaveBestDistance -= OnSaveBestDistance;
        _localizationManager.SaveLanguageIndex -= OnSaveLanguageIndex;
        _volumeChanger.SaveVolume -= OnSaveVolume;
    }

    public void OnSaveBestDistance(int value)
    {
        _save.BestDistance = value;
    }

    public void OnSaveLanguageIndex(int value)
    {
        _save.LanguageIndex = value;
    }

    public void OnSaveVolume(float value)
    {
        _save.Volume = value;
    }

#if UNITY_ANDROID && !UNITY_EDITOR
    private void OnApplicationPause(bool pause)
    {
        if (pause) File.WriteAllText(_path, JsonUtility.ToJson(_save));
    }
#endif
    private void OnApplicationQuit()
    {
        File.WriteAllText(_path, JsonUtility.ToJson(_save));
    }
}

[Serializable]
public class Save
{
    public int BestDistance;
    public int LanguageIndex;
    public float Volume;
}