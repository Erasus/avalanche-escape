﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private SwipeInput _swipeInput;

    public event UnityAction MoveRight;
    public event UnityAction MoveLeft;

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            MoveLeft?.Invoke();

        if (Input.GetKeyDown(KeyCode.RightArrow))
            MoveRight?.Invoke();
    }
#endif

    private void OnEnable()
    {
        _swipeInput.RightSwipe += OnRightSwipe;
        _swipeInput.LeftSwipe += OnLeftSwipe;
    }

    private void OnDisable()
    {
        _swipeInput.RightSwipe -= OnRightSwipe;
        _swipeInput.LeftSwipe -= OnLeftSwipe;
    }

    private void OnRightSwipe()
    {
        MoveRight?.Invoke();
    }

    private void OnLeftSwipe()
    {
        MoveLeft?.Invoke();
    }
}
