﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;

[RequireComponent(typeof(GameModeSwitcher))]
public class AdvertisementsVideo : MonoBehaviour
{
    [SerializeField] private string myPlacementId = "Interstitial_Android";
    [SerializeField] private bool testMode = true;
    [SerializeField] private int _gameOverCount;

#if UNITY_IOS
    private string gameId = "4226000";
#elif UNITY_ANDROID
    private string gameId = "4226001";
#endif
    private GameModeSwitcher _gameModeSwitcher;

    private void Awake()
    {
        _gameModeSwitcher = GetComponent<GameModeSwitcher>();
        Advertisement.Initialize(gameId, testMode);
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameOver -= OnGameOver;
    }

    private void OnGameOver()
    {
        if(_gameModeSwitcher.GameOverCount >= _gameOverCount)
        {
            ShowInterstitialAd();
            _gameModeSwitcher.ResetGameOverCounter();
        }
    }

    private void ShowInterstitialAd()
    {
        if (Advertisement.IsReady(myPlacementId))
        {
            Advertisement.Show(myPlacementId);
        }
    }
}
