﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject _container;
    [SerializeField] private int _capacity;
    [SerializeField] private Obstacle[] _templates;
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;

    private Camera _camera;
    private List<Obstacle> _obstaclePool = new List<Obstacle>();

    private void Start()
    {
        Initialize(_templates);
    }

    private void Initialize(Obstacle[] prefabs)
    {
        _camera = Camera.main;

        for (int i = 0; i < prefabs.Length; i++)
        {
            for (int j = 0; j < _capacity; j++)
            {
                Obstacle spawned = Instantiate(prefabs[i], _container.transform);
                spawned.gameObject.SetActive(false);

                _obstaclePool.Add(spawned);
            }
        }

        ShufflePool();
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GameOver += OnGameOver;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GameOver -= OnGameOver;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
    }

    private void OnGameBegin()
    {
        HideObstaclePool();
    }

    private void OnGameOver()
    {
        StopMoovingObstaclePool();
    }

    private void OnReturningStartScreen()
    {
        HideObstaclePool();
        StopMoovingObstaclePool();
    }

    public bool TryGetObject(out Obstacle result, ObstacleType obstacleType = ObstacleType.NonKnockableObstacle)
    {
        result = _obstaclePool.FirstOrDefault(p => p.gameObject.activeSelf == false && p.ObstacleType == obstacleType);

        return result != null;
    }

    private void HideObstaclePool()
    {
        foreach (var obstacle in _obstaclePool)
        {
            if(obstacle.isActiveAndEnabled)
            {
                obstacle.gameObject.SetActive(false);
            }

            obstacle.ResetSpeed();
        }
    }
    
    private void StopMoovingObstaclePool()
    {
        foreach (var obstacle in _obstaclePool)
        {
            if (obstacle.isActiveAndEnabled)
                obstacle.StopSpeed();
        }
    }

    public void DisableObjectAbroadScreen()
    {
        Vector3 disablePoint = _camera.ViewportToWorldPoint(new Vector2(0, 1.3f));

        foreach (var _obstacle in _obstaclePool)
        {
            if (_obstacle.gameObject.activeSelf == true)
            {
                if (Mathf.Abs(_obstacle.transform.position.y) > disablePoint.y)
                    _obstacle.gameObject.SetActive(false);
            }
        }
    }

    public void UpSpeedOfObstaclePool(float accelerationCoefficient)
    {
        foreach (var obstacle in _obstaclePool)
        {
            obstacle.UpSpeed(accelerationCoefficient);
        }
    }

    public void ShufflePool()
    {
        for (int i = _obstaclePool.Count - 1; i >= 1; i--)
        {
            int j = Random.Range(0, i);

            var tmp = _obstaclePool[j];
            _obstaclePool[j] = _obstaclePool[i];
            _obstaclePool[i] = tmp;
        }
    }
}
