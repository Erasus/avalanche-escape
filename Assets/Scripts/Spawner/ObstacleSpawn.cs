﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(ObjectPool))]
public class ObstacleSpawn : MonoBehaviour
{
    [SerializeField] private DistanceCounter _distanceCounter;
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [Header("Параметры обычных препятствий")]
    [SerializeField] private Transform[] _downSpawnPoints;
    [SerializeField] private float _secondsBetweenSpawnObstacle;
    [SerializeField] private float _reducingTimeBetweenSpawnSnowBall;
    [SerializeField] private float _timeToStartSpawn;
    [SerializeField, Range(0, 1)] private float _minSecondsBetweenSpawnObstacle;
    [SerializeField, Range(0, 100)] private int _createDoubleObstacleChance;
    [Header("Параметры снежных шаров")]
    [SerializeField] private Transform[] _upSpawnPoints;
    [SerializeField] private Image[] _atentionPoints;
    [SerializeField] private float _secondsBetweenSpawnSnowBall;
    [SerializeField] private float _atentionTime;

    private ObjectPool _objectPool;
    private float _elapsedTimeForObstacle = 0;
    private float _elapsedTimeForSnowBall = 0;
    private float _lastSecondsBetweenSpawnObstacle;
    private float _lastSecondsBetweenSpawnSnowBall;
    private bool _gameStarted = false;
    private bool _startGameCoroutineIsBegin = false;
    private IEnumerator _startGameCoroutine;
    private bool _startSpawnSnowBallCoroutineIsBegin = false;
    private IEnumerator _startSpawnSnowBallCoroutine;

    public event UnityAction PlayAtention;

    private void Start()
    {
        _objectPool = GetComponent<ObjectPool>();
        ResetTimeBetweenSpawn();
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GameOver += OnGameOver;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _distanceCounter.SpeedIncreased += OnSpeedIncreased;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GameOver -= OnGameOver;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _distanceCounter.SpeedIncreased -= OnSpeedIncreased;
    }

    private void Update()
    {
        if (_gameStarted)
        {
            SpawnObstacle();
            SpawnSnowBall();
        }
    }

    private void SpawnObstacle()
    {
        _elapsedTimeForObstacle += Time.deltaTime;

        if (_elapsedTimeForObstacle >= _lastSecondsBetweenSpawnObstacle)
        {
            if (_objectPool.TryGetObject(out Obstacle obstacle))
            {
                _elapsedTimeForObstacle = 0;

                int spawnPointNumber = Random.Range(0, _downSpawnPoints.Length);

                SetObstacle(obstacle, _downSpawnPoints[spawnPointNumber].position);

                if (Random.Range(0, 100) <= _createDoubleObstacleChance)
                {
                    GetSecondObslacleFromPool(spawnPointNumber);
                }

                _objectPool.DisableObjectAbroadScreen();
            }
        }
    }

    private void SetObstacle(Obstacle obstacle, Vector3 spawnPoint)
    {
        obstacle.gameObject.SetActive(true);
        obstacle.transform.position = spawnPoint;
    }

    private void GetSecondObslacleFromPool(int position)
    {
        if (_objectPool.TryGetObject(out Obstacle obstacle, ObstacleType.KnockableObstacle))
        {
            SetObstacle(obstacle, _downSpawnPoints[ChooseSecondObstaclePosition(position)].position);
        }
    }

    private int ChooseSecondObstaclePosition(int index)
    {
        switch (index)
        {
            case 0:
            default:
                return Random.Range(index + 1, _downSpawnPoints.Length);

            case 1:
                return Random.Range(0, index + 1) < index ? 0 : index + 1;

            case 2:
                return Random.Range(0, index);
        }
    }

    private void SpawnSnowBall()
    {
        _elapsedTimeForSnowBall += Time.deltaTime;

        if (_elapsedTimeForSnowBall >= _lastSecondsBetweenSpawnSnowBall)
        {
            if (_objectPool.TryGetObject(out Obstacle obstacle, ObstacleType.SnowBoulder))
            {
                _elapsedTimeForSnowBall = 0;
                int spawnPointNumber = Random.Range(0, _upSpawnPoints.Length);
                _startSpawnSnowBallCoroutine = ShowBallAtention(obstacle, _upSpawnPoints[spawnPointNumber].position, spawnPointNumber);

                StartCoroutine(_startSpawnSnowBallCoroutine);

                _objectPool.DisableObjectAbroadScreen();
            }
        }
    }

    private IEnumerator ShowBallAtention(Obstacle obstacle, Vector3 spawnPoint, int index)
    {
        _startSpawnSnowBallCoroutineIsBegin = true;
        _atentionPoints[index].gameObject.SetActive(true);
        PlayAtention?.Invoke();

        yield return new WaitForSeconds(_atentionTime);

        _atentionPoints[index].gameObject.SetActive(false);

        SetObstacle(obstacle, spawnPoint);
        _startSpawnSnowBallCoroutineIsBegin = false;
    }

    private void OnGameBegin()
    {
        _startGameCoroutine = StartSpawnDelay();
        StartCoroutine(_startGameCoroutine);
    }

    private IEnumerator StartSpawnDelay()
    {
        _startGameCoroutineIsBegin = true;
        yield return new WaitForSeconds(_timeToStartSpawn);
        _gameStarted = true;
        _startGameCoroutineIsBegin = false;
    }

    private void OnGameOver()
    {
        _gameStarted = false;
        _elapsedTimeForObstacle = 0;
        _elapsedTimeForSnowBall = 0;
        _objectPool.ShufflePool();
        ResetTimeBetweenSpawn();

        if (_startGameCoroutineIsBegin)
        {
            StopCoroutine(_startGameCoroutine);
            _startGameCoroutineIsBegin = false;
        }
        
        if (_startSpawnSnowBallCoroutineIsBegin)
        {
            StopCoroutine(_startSpawnSnowBallCoroutine);
            _startSpawnSnowBallCoroutineIsBegin = false;

            foreach (var atention in _atentionPoints)
            {
                if (atention.isActiveAndEnabled)
                    atention.gameObject.SetActive(false);
            }
        }
    }

    private void OnReturningStartScreen()
    {
        OnGameOver();
    }

    private void ResetTimeBetweenSpawn()
    {
        _lastSecondsBetweenSpawnObstacle = _secondsBetweenSpawnObstacle;
        _lastSecondsBetweenSpawnSnowBall = _secondsBetweenSpawnSnowBall;
    }

    private void OnSpeedIncreased(float accelerationCoefficient)
    {
        _objectPool.UpSpeedOfObstaclePool(accelerationCoefficient);
        ReduceTimeBetweenSpawnObstacle();
    }

    private void ReduceTimeBetweenSpawnObstacle()
    {
        if ((_lastSecondsBetweenSpawnObstacle - _reducingTimeBetweenSpawnSnowBall) > _minSecondsBetweenSpawnObstacle)
            _lastSecondsBetweenSpawnObstacle -=_reducingTimeBetweenSpawnSnowBall;
    }
}
