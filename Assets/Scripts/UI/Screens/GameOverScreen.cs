﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(GameOverScreenStatistic))]
public class GameOverScreen : Screen
{
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _exitGameScreenButton;
    [SerializeField] private float _panelAppearanceSpeed;
    [SerializeField] private float _timeUntilOpenScreen;

    private GameOverScreenStatistic _gameOverScreenStatistic;

    public event UnityAction RestartButtonClick;
    public event UnityAction OpenOptionsButtonClick;
    public event UnityAction OpenExitScreenButtonClick;

    private void Awake()
    {
        _gameOverScreenStatistic = GetComponent<GameOverScreenStatistic>();
    }

    protected new void OnEnable()
    {
        base.OnEnable();
        _optionsButton.onClick.AddListener(OpenOptionsScreen);
        _exitGameScreenButton.onClick.AddListener(OpenExitScreen);
    }

    protected new void OnDisable()
    {
        base.OnDisable();
        _optionsButton.onClick.RemoveListener(OpenOptionsScreen);
        _exitGameScreenButton.onClick.RemoveListener(OpenExitScreen);
    }

    private void OpenOptionsScreen()
    {
        ScreenOpened = true;
        Close();

        OpenOptionsButtonClick?.Invoke();
    }

    private void OpenExitScreen()
    {
        ScreenOpened = true;
        Close();

        OpenExitScreenButtonClick?.Invoke();
    }

    public override void Close()
    {
        CanvasGroup.alpha = 0;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;
        SwipePanel.raycastTarget = true;
    }

    public override void Open()
    {
        ScreenOpened = false;
        _gameOverScreenStatistic.ShowBestScore();

        StartCoroutine(ChangeScreenAlpha());
    }

    private IEnumerator ChangeScreenAlpha()
    {
        yield return new WaitForSeconds(_timeUntilOpenScreen);

        while(CanvasGroup.alpha < 1)
        {
            CanvasGroup.alpha += _panelAppearanceSpeed * Time.deltaTime;
            yield return null;
        }

        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;
        SwipePanel.raycastTarget = false;
    }

    protected override void OnButtonClick()
    {
        RestartButtonClick?.Invoke();
    }
}
