﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScreenStatistic : Statistic
{
    public override void ShowBestScore()
    {
        if (DistanceCounter.BestDistance > 0)
        {
            BestScoreImage.gameObject.SetActive(true);
            BestDistanceText.text = DistanceCounter.BestDistance.ToString() + "m.";
        }
        else
        {
            BestScoreImage.gameObject.SetActive(false);
        }
    }
}
