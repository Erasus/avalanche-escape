﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerInput))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [SerializeField] private DistanceCounter _distanceCounter;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _standartAccelerationCoefficient = 1;
    [SerializeField] private Vector2 _stepSize;
    [SerializeField] private float _maxWidth;
    [SerializeField] private float _minWidth;
    [SerializeField] private Vector2 _disabledPosition;

    private PlayerInput _playerInput;
    private float _lastAccelerationCoefficient;
    private Vector3 _startPosition;
    private Vector3 _targetPosition;
    private bool _ride = false;
    private bool _rideStarted = false;
    private bool _endChangePosition = false;
    private bool _firstRide = true;

    public event UnityAction MoveCenter;
    public event UnityAction MoveLeft;
    public event UnityAction MoveRight;
    public event UnityAction FirstRide;

    private void Awake()
    {
        _playerInput = GetComponent<PlayerInput>();
        _startPosition = _targetPosition = transform.position;
        ResetSpeed();
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GameOver += OnGameOver;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _gameModeSwitcher.PlaceOnDisabledPosition += OnPlaceOnDisabledPosition;
        _distanceCounter.SpeedIncreased += OnSpeedIncreased;
        _playerInput.MoveLeft += OnMoveLeft;
        _playerInput.MoveRight += OnMoveRight;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GameOver -= OnGameOver;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _gameModeSwitcher.PlaceOnDisabledPosition -= OnPlaceOnDisabledPosition;
        _distanceCounter.SpeedIncreased -= OnSpeedIncreased;
        _playerInput.MoveLeft -= OnMoveLeft;
        _playerInput.MoveRight -= OnMoveRight;
    }

    private void OnSpeedIncreased(float accelerationCoefficient)
    {
        _lastAccelerationCoefficient += accelerationCoefficient;
    }

    private void Update()
    {
        if (transform.position != _targetPosition && _rideStarted)
            Move(_targetPosition);

        if(transform.position == _targetPosition && _endChangePosition)
        {
            MoveCenter?.Invoke();
            _endChangePosition = false;
        }

        if (transform.position == _targetPosition && _rideStarted && _ride == false)
            _ride = true;
    }

    private void Move(Vector3 _position)
    {
        transform.position = Vector3.MoveTowards(transform.position, _position, _moveSpeed * Time.deltaTime * _lastAccelerationCoefficient);

        if (_endChangePosition == false)
           _endChangePosition = true;
    }

    private void OnMoveLeft()
    {
        TryMoveLeft();
    }

    private void TryMoveLeft()
    {
        if(_targetPosition.x > _minWidth && _ride)
        {
            SetNextPosition(-_stepSize.x, -_stepSize.y);
            MoveLeft?.Invoke();
        }
    }

    private void OnMoveRight()
    {
        TryMoveRight();
    }
    
    private void TryMoveRight()
    {
        if (_targetPosition.x < _maxWidth && _ride)
        {
            SetNextPosition(_stepSize.x, _stepSize.y);
            MoveRight?.Invoke();
        }
    }

    private void SetNextPosition(float stepSizeX, float stepSizeY)
    {
        _targetPosition = new Vector2(_targetPosition.x + stepSizeX, _targetPosition.y + stepSizeY);
    }

    private void OnPlaceOnDisabledPosition()
    {
        transform.position = _disabledPosition;
        MoveCenter?.Invoke();
    }

    private void OnGameBegin()
    {
        _rideStarted = true;

        if(_firstRide)
        {
            _firstRide = false;
            FirstRide?.Invoke();
        }

        _targetPosition = _startPosition;
    }

    private void OnGameOver()
    {
        _ride = false;
        _rideStarted = false;
        ResetSpeed();
    }

    private void OnReturningStartScreen()
    {
        OnGameOver();
    }

    private void ResetSpeed()
    {
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }
}
