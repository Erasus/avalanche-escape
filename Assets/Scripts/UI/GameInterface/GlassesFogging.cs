﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlassesFogging : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [SerializeField] private Image _screenFoggingOfGlasses;
    [SerializeField] private Button _glassesCleaningButton;
    [SerializeField] private Image _disablebGlassesImage;
    [SerializeField] private float _standartGlassesCleaningButtonCooldown;
    [SerializeField] private float _timeBeforeFoggingGlasses;
    [SerializeField] private float _foggingSpeed;
    [SerializeField] private float _foggingCrashSpeed;

    private Material _screenFoggingOfGlassesMaterial;
    private float _lastGlassesCleaningButtonCooldown;
    private IEnumerator _foggingCorutine;
    private bool _foggingCorutineIsBegin = false;
    private bool _noWaitTimeForFogging = false;
    private bool _gameStarted = false;
    private float _foggedValue;

    private void Start()
    {
        _screenFoggingOfGlassesMaterial = _screenFoggingOfGlasses.material;
        _foggedValue = 0;
        _screenFoggingOfGlassesMaterial.SetFloat("_Fade", _foggedValue);
        _lastGlassesCleaningButtonCooldown = _standartGlassesCleaningButtonCooldown;
    }

    private void OnEnable()
    {
        _glassesCleaningButton.onClick.AddListener(CleanGlasses);
        _player.CrashedIntoSnowdrift += OnCrashedIntoSnowdrift;
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _gameModeSwitcher.GameOver += OnGameOver;
    }

    private void OnDisable()
    {
        _glassesCleaningButton.onClick.RemoveListener(CleanGlasses);
        _player.CrashedIntoSnowdrift -= OnCrashedIntoSnowdrift;
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _gameModeSwitcher.GameOver -= OnGameOver;
    }

    private void Update()
    {
        if (_gameStarted)
        {
            CleanGlassesCooldown();
        }
    }

    private void StartFoggingCorutine()
    {
        if (_foggingCorutineIsBegin == false)
        {
            _foggingCorutine = Fogging();
            StartCoroutine(_foggingCorutine);
        }
    }

    private IEnumerator Fogging()
    {
        _foggingCorutineIsBegin = true;

        if (_noWaitTimeForFogging == false)
            yield return new WaitForSeconds(_timeBeforeFoggingGlasses);
        else
            _noWaitTimeForFogging = false;

        while (_foggedValue < 1)
        {
            _foggedValue += Time.deltaTime * _foggingSpeed;
            _screenFoggingOfGlassesMaterial.SetFloat("_Fade", _foggedValue);

            yield return null;
        }

        _foggingCorutineIsBegin = false;
    }

    private IEnumerator CrashFogging(float value)
    {
        _foggingCorutineIsBegin = true;
        float deltaFillAmoun = _foggedValue + value > 1? 1 : _foggedValue + value;

        while (_foggedValue < deltaFillAmoun)
        {
            _foggedValue += Time.deltaTime * _foggingCrashSpeed;
            _screenFoggingOfGlassesMaterial.SetFloat("_Fade", _foggedValue);

            yield return null;
        }

        _foggingCorutineIsBegin = false;
        _noWaitTimeForFogging = true;

        StartFoggingCorutine();
    }

    private void StopFoggingCorutine()
    {
        if (_foggingCorutineIsBegin == true)
        {
            StopCoroutine(_foggingCorutine);
            _foggingCorutineIsBegin = false;
        }
    }

    private void CleanGlasses()
    {
        StopFoggingCorutine();

        _foggedValue = 0;
        _screenFoggingOfGlassesMaterial.SetFloat("_Fade", _foggedValue);
        _glassesCleaningButton.interactable = false;
        _lastGlassesCleaningButtonCooldown = _standartGlassesCleaningButtonCooldown;

        if (_gameStarted == true)
            StartFoggingCorutine();
    }

    private void CleanGlassesCooldown()
    {
        _lastGlassesCleaningButtonCooldown -= Time.deltaTime;
        _disablebGlassesImage.fillAmount = _lastGlassesCleaningButtonCooldown / _standartGlassesCleaningButtonCooldown;

        if (_lastGlassesCleaningButtonCooldown <= 0)
        {
            _glassesCleaningButton.interactable = true;
        }
    }

    private void OnCrashedIntoSnowdrift(float damage)
    {
        StopFoggingCorutine();

        if (_foggingCorutineIsBegin == false)
        {
            _foggingCorutine = CrashFogging(damage);
            StartCoroutine(_foggingCorutine);
        }
    }

    private void OnGameBegin()
    {
        _gameStarted = true;
        StartFoggingCorutine();
    }

    private void OnReturningStartScreen()
    {
        CompleteUnfogging();
    }

    private void OnGameOver()
    {
        CompleteUnfogging();
    }

    private void CompleteUnfogging()
    {
        _gameStarted = false;
        StopFoggingCorutine();
        CleanGlasses();
    }
}
