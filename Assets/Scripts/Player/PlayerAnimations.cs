﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Player), typeof(PlayerMovement))]
public class PlayerAnimations : MonoBehaviour
{
    private Animator _animator;
    private Player _player;
    private PlayerMovement _playerMovement;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _player = GetComponentInParent<Player>();
        _playerMovement = GetComponentInParent<PlayerMovement>();
    }

    private void OnEnable()
    {
        _player.Crashed += OnCrashed;
        _playerMovement.MoveCenter += OnMoveCenter;
        _playerMovement.MoveLeft += OnMoveLeft;
        _playerMovement.MoveRight += OnMoveRight;
        _playerMovement.FirstRide += OnFirstRide;
    }

    private void OnDisable()
    {
        _player.Crashed -= OnCrashed;
        _playerMovement.MoveCenter -= OnMoveCenter;
        _playerMovement.MoveLeft -= OnMoveLeft;
        _playerMovement.MoveRight -= OnMoveRight;
        _playerMovement.FirstRide -= OnFirstRide;
    }

    private void OnCrashed()
    {
        _animator.Play("Fall");
    }

    private void OnMoveCenter()
    {
        _animator.Play("Move");
    }

    private void OnMoveLeft()
    {
        _animator.Play("MoveLeft");
    }
    
    private void OnMoveRight()
    {
        _animator.Play("MoveRight");
    }

    private void OnFirstRide()
    {
        _animator.Play("StandUP");
    }
}
