﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ExitGameScreen : Screen
{
    [SerializeField] private Button _exitGameButton;

    public event UnityAction CloseExitGameScreenButtonClick;

    protected new void OnEnable()
    {
        base.OnEnable();
        _exitGameButton.onClick.AddListener(ExitGame);
    }

    protected new void OnDisable()
    {
        base.OnDisable();
        _exitGameButton.onClick.RemoveListener(ExitGame);
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    public override void Close()
    {
        CanvasGroup.alpha = 0;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;
    }

    public override void Open()
    {
        CanvasGroup.alpha = 1;
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;
    }

    protected override void OnButtonClick()
    {
        CloseExitGameScreenButtonClick?.Invoke();
    }
}
