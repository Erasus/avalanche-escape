﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class KnockableCollision : MonoBehaviour
{
    [SerializeField, Range(0, 1)] private float _damage;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Player player))
        {
            player.SmashSnowdrift(_damage);

            _animator.Play("Blow");
        }
    }

    public void Blow()
    {
        gameObject.SetActive(false);
    }
}
