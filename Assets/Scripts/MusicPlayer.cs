﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameModeSwitcher), typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour
{
    [Header("Звуковые эффекты игрока")]
    [SerializeField] private Player _player;
    [SerializeField] private AudioSource _playerAudioSource;
    [Space(15), Header("Звуковые эффекты игры")]
    [SerializeField] private AudioClip _rideMusic;
    [SerializeField] private AudioClip _menuMusic;
    [Space(15), Header("Звуковые эффекты игровых предупреждений")]
    [SerializeField] private ObstacleSpawn _obstacleSpawn;
    [SerializeField] private AudioSource _atentionAudioSource;

    private GameModeSwitcher _gameModeSwitcher;
    private AudioSource _gameAudioSource;

    private void Awake()
    {
        _gameModeSwitcher = GetComponent<GameModeSwitcher>();
        _gameAudioSource = GetComponent<AudioSource>();

        PlayMenuMusic();
    }

    private void OnEnable()
    {
        _player.Crashed += OnCrashed;
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _gameModeSwitcher.GamePaused += OnGamePaused;
        _gameModeSwitcher.GameUnpaused += OnGameUnpaused;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
        _gameModeSwitcher.GameOver += OnGameOver;
        _obstacleSpawn.PlayAtention += OnPlayAtention;
    }

    private void OnDisable()
    {
        _player.Crashed -= OnCrashed;
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _gameModeSwitcher.GamePaused -= OnGamePaused;
        _gameModeSwitcher.GameUnpaused -= OnGameUnpaused;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
        _gameModeSwitcher.GameOver -= OnGameOver;
        _obstacleSpawn.PlayAtention -= OnPlayAtention;
    }

    private void OnCrashed()
    {
        _playerAudioSource.Play();
        PlayMenuMusic();
    }

    private void OnGameBegin()
    {
        PlayRideMusic();
    }

    private void PlayRideMusic()
    {
        _gameAudioSource.clip = _rideMusic;
        _gameAudioSource.Play();
    }

    private void PlayMenuMusic()
    {
        _gameAudioSource.clip = _menuMusic;
        _gameAudioSource.Play();
    }

    private void OnGamePaused()
    {
        _gameAudioSource.Pause();
    }
    
    private void OnGameUnpaused()
    {
        _gameAudioSource.UnPause();
    }

    private void OnReturningStartScreen()
    {
        PlayMenuMusic();
    }

    private void OnGameOver()
    {
        PlayMenuMusic();
    }

    private void OnPlayAtention()
    {
        _atentionAudioSource.Play();
    }
}
