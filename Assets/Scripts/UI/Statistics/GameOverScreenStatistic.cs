﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreenStatistic : Statistic
{
    [SerializeField] private TMP_Text _distanceCoveredText;
    [SerializeField] private Image _distanceCoveredImage;
    [SerializeField] private Image _newBestScoreImage;

    public override void ShowBestScore()
    {
        _distanceCoveredText.text = DistanceCounter.Distance.ToString() + "м.";

        if (DistanceCounter.CheckNewHighscore())
        {
            _newBestScoreImage.gameObject.SetActive(true);
            BestScoreImage.gameObject.SetActive(false);
        }
        else if (DistanceCounter.BestDistance > 0)
        {
            _newBestScoreImage.gameObject.SetActive(false);
            BestScoreImage.gameObject.SetActive(true);
            BestDistanceText.text = DistanceCounter.BestDistance.ToString() + "м.";
        }
        else
        {
            _newBestScoreImage.gameObject.SetActive(false);
            BestScoreImage.gameObject.SetActive(false);
        }
    }
}
