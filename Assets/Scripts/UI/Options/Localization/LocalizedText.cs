﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class LocalizedText : MonoBehaviour
{
    [SerializeField] private LocalizationManager _localizationManager;

    private TMP_Text _text;
    private string _key;

    private void Start()
    {
        Localize();
    }

    private void OnEnable()
    {
        _localizationManager.LanguageChange += OnLanguageChange;
    }

    private void OnDisable()
    {
        _localizationManager.LanguageChange -= OnLanguageChange;
    }

    private void OnLanguageChange()
    {
        Localize();
    }

    private void Init()
    {
        _text = GetComponent<TMP_Text>();
        _key = _text.text;
    }

    private void Localize(string newKey = null)
    {
        if (_text == null)
            Init();

        if (newKey != null)
            _key = newKey;

        _text.text = _localizationManager.GetTranslate(_key);
    }
}
