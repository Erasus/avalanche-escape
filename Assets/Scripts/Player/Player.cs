﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public event UnityAction Crashed;
    public event UnityAction<float> CrashedIntoSnowdrift;

    public void Die()
    {
        Crashed?.Invoke();
    }

    public void SmashSnowdrift(float damage)
    {
        CrashedIntoSnowdrift?.Invoke(damage);
    }
}
