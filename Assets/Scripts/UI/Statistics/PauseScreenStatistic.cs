﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PauseScreenStatistic : Statistic
{
    [SerializeField] private TMP_Text _distanceCoveredText;

    public override void ShowBestScore()
    {
        _distanceCoveredText.text = DistanceCounter.Distance.ToString() + "м.";

        if (DistanceCounter.BestDistance > 0)
        {
            BestScoreImage.gameObject.SetActive(true);
            BestDistanceText.text = DistanceCounter.BestDistance.ToString() + "м.";
        }
        else
        {
            BestScoreImage.gameObject.SetActive(false);
        }
    }
}
