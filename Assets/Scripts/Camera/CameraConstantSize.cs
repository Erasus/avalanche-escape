﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraConstantSize : MonoBehaviour
{
    [SerializeField] private Vector2 _defaultResolution = new Vector2(1080, 1920);
    [SerializeField, Range(0f, 1f)] private float _widthOrHeight = 0;

    private Camera _camera;
    private float _initialSize;
    private float _targetAspect;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _initialSize = _camera.orthographicSize;
        _targetAspect = _defaultResolution.x / _defaultResolution.y;

        float constantSize = _initialSize * (_targetAspect / _camera.aspect);
        _camera.orthographicSize = Mathf.Lerp(constantSize, _initialSize, _widthOrHeight);
    }
}
