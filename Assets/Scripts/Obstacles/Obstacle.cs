﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObstacleMover))]
public class Obstacle : MonoBehaviour
{
    [SerializeField] private ObstacleType _obtacleType;

    private ObstacleMover _obstacleMover;

    public ObstacleType ObstacleType => _obtacleType;

    private void Awake()
    {
        _obstacleMover = GetComponent<ObstacleMover>();
    }

    public void UpSpeed(float accelerationCoefficient)
    {
        _obstacleMover.UpAccelerationCoefficient(accelerationCoefficient);
    }

    public void ResetSpeed()
    {
        _obstacleMover.ResetSpeed();
    }
    
    public void StopSpeed()
    {
        _obstacleMover.StopMooving();
    }
}

public enum ObstacleType
{
    NonKnockableObstacle,
    KnockableObstacle,
    SnowBoulder
}
