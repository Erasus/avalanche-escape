﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SwipeInput : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    public event UnityAction RightSwipe;
    public event UnityAction LeftSwipe;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            if (eventData.delta.x > 0)
                RightSwipe?.Invoke();
            else
                LeftSwipe?.Invoke();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }
}
