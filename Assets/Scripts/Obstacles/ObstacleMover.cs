﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMover : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _standartAccelerationCoefficient = 1;

    private float _lastAccelerationCoefficient;
    private float _standartSpeed;

    private void Awake()
    {
        _standartSpeed = _speed;
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }

    private void Update()
    {
        transform.Translate(Vector3.up * _speed * _lastAccelerationCoefficient * Time.deltaTime);
    }

    public void UpAccelerationCoefficient(float accelerationCoefficient)
    {
        _lastAccelerationCoefficient += accelerationCoefficient;
    }

    public void ResetSpeed()
    {
        _speed = _standartSpeed;
        _lastAccelerationCoefficient = _standartAccelerationCoefficient;
    }

    public void StopMooving()
    {
        _speed = 0;
    }
}
