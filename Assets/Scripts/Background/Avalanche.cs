﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Avalanche : MonoBehaviour
{
    [SerializeField] private GameModeSwitcher _gameModeSwitcher;
    [SerializeField] private Player _player;
    [SerializeField] private Vector3 _gamePosition;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private Vector3 _gameOverPosition;
    [SerializeField] private float _timeUntilAvalanche;

    private Vector3 _disabledPosition;
    private Vector3 _targetPosition;

    private void Awake()
    {
        _disabledPosition = _targetPosition = transform.position;
    }

    private void OnEnable()
    {
        _gameModeSwitcher.GameBegin += OnGameBegin;
        _player.Crashed += OnCrashed;
        _gameModeSwitcher.ReturningStartScreen += OnReturningStartScreen;
    }

    private void OnDisable()
    {
        _gameModeSwitcher.GameBegin -= OnGameBegin;
        _player.Crashed -= OnCrashed;
        _gameModeSwitcher.ReturningStartScreen -= OnReturningStartScreen;
    }

    private void Update()
    {
        if(transform.position != _targetPosition)
        {
            Move(_targetPosition);
        }
    }

    private void Move(Vector2 _position)
    {
        transform.position = Vector3.MoveTowards(transform.position, _position, _moveSpeed * Time.deltaTime);
    }

    private void OnGameBegin()
    {
        PlaceOnDisabledPosition();
        StartCoroutine(Avalanching(_gamePosition));
    }

    private void OnCrashed()
    {
        StartCoroutine(Avalanching(_gameOverPosition));
    }

    private IEnumerator Avalanching(Vector3 position)
    {
        yield return new WaitForSeconds(_timeUntilAvalanche);

        _targetPosition = position;
    }

    private void OnReturningStartScreen()
    {
        PlaceOnDisabledPosition();
    }

    private void PlaceOnDisabledPosition()
    {
        transform.position = _targetPosition = _disabledPosition;
    }
}
